        <div class="thumbnail thumb-shadow">
            <div class="property-header">
                <a href="<?php echo site_url('property/'.$row->unique_id.'/'.dbc_url_title($title));?>"></a>
                <img class="property-header-image" src="<?php echo get_featured_photo_by_id($row->featured_img);?>" alt="<?php echo character_limiter($title,20);?>" style="width:256px">
                <?php if($row->estate_condition=='DBC_CONDITION_SOLD'){?>
                <span class="property-contract-type sold"><span><?php echo lang_key('DBC_CONDITION_SOLD'); ?></span>
                    <?php }elseif($row->estate_condition=='DBC_CONDITION_RENTED'){?>
                    <span class="property-contract-type sold"><span><?php echo lang_key('DBC_CONDITION_RENTED'); ?></span>
                        <?php }elseif($row->purpose=='DBC_PURPOSE_SALE'){?>
                        <span class="property-contract-type sale"><span><?php echo lang_key('DBC_PURPOSE_SALE'); ?></span>
                            <?php }else if($row->purpose=='DBC_PURPOSE_RENT'){?>
                            <span class="property-contract-type rent"><span><?php echo lang_key('DBC_PURPOSE_RENT'); ?></span>
                                <?php }else if($row->purpose=='DBC_PURPOSE_BOTH'){?>
                                <span class="property-contract-type both"><span style="font-size: 11px"><?php echo lang_key('DBC_PURPOSE_BOTH'); ?></span>
                                    <?php }?>
                      </span>
                            <div class="property-thumb-meta">
                                <span class="property-price"><?php echo show_price($row->total_price,$row->id);?></span>
                            </div>
            </div>
            <div class="caption">
                <h4 class="estate-title"><?php echo character_limiter($title,20);?></h4>
                <p class="estate-description"><?php echo get_location_name_by_id($row->city).','.get_location_name_by_id($row->state).','.get_location_name_by_id($row->country);?></p>

                <div class="grid-list">
                    <span class="rtl-right left"><?php echo lang_key('type'); ?>:</span>
                    <span class="rtl-left right"><?php echo lang_key($row->type);?></span>
                </div>
                <div class="grid-list">
                    <span class="rtl-right" style="float:left; font-weight:bold;"><?php echo lang_key('area'); ?>:</span>
                    <?php if($row->type=='DBC_TYPE_LAND'){?>
                        <span class="rtl-left" style="float:right; "><?php echo (int)$row->lot_size;?> <?php echo show_square_unit($row->lot_size_unit);?></span>
                    <?php }else{?>
                        <span class="rtl-left" style="float:right; "><?php echo (int)$row->home_size;?> <?php echo show_square_unit($row->home_size_unit);?></span>
                    <?php }?>
                </div>
                <div class="grid-list property-utilities">
                    <div title="Bathrooms" class="bathrooms rtl-left">
                        <?php if($row->type=='DBC_TYPE_LAND'){?>
                            <div class="content">N/A</div>
                        <?php }else{?>
                            <div class="content"><?php echo $row->bath;?></div>
                        <?php }?>
                    </div>
                    <div title="Bedrooms" class="bedrooms  rtl-right">
                        <?php if($row->type=='DBC_TYPE_LAND'){?>
                            <div class="content">N/A</div>
                        <?php }else{?>
                            <div class="content"><?php echo $row->bedroom;?></div>
                        <?php }?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="grid-divider"></div>
                <p>
                    <a href="<?php echo site_url('property/'.$row->unique_id.'/'.dbc_url_title($title));?>" class="btn btn-primary  btn-labeled">
                        <?php echo lang_key('details'); ?>
                        <span class="btn-label btn-label-right">
                                       <i class="fa fa-arrow-right"></i>
                                    </span>
                    </a>
                </p>
            </div>
        </div>
