<script src="<?php echo theme_url();?>/assets/js/sample_map_data.js"></script>
<script src="<?php echo theme_url();?>/assets/js/map_config.js"></script>
<script type="text/javascript">
    var map;
    var markers = [];
    var markerCluster;
    function result_js(result)
    {


        var ul = document.getElementById("marker_list");
        ul.innerHTML = '';
            pointMap(result.data);
          $('#estate-count').html(result.count);

    }
    var iconBase = '<?php echo theme_url();?>/assets/images/map-icons/';
    var myLatitude = parseFloat('<?php echo get_settings('banner_settings','map_latitude', 37.2718745); ?>');
    var myLongitude = parseFloat('<?php echo get_settings('banner_settings','map_longitude', -119.2704153); ?>');
    var zoomLevel = parseInt('<?php echo get_settings('banner_settings','map_zoom',8); ?>');


    function initialize() {
        var myLatlng = new google.maps.LatLng(myLatitude,myLongitude);
        var mapOptions = {
            scrollwheel: false,
            zoom: zoomLevel,
            center: myLatlng,
            mapTypeControl: true,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.RIGHT_BOTTOM
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            panControl: true,
            panControlOptions: {
                position: google.maps.ControlPosition.RIGHT_TOP
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: MAP_STYLE
        }
        map = new google.maps.Map(document.getElementById('main-map'), mapOptions);

        google.maps.event.addListener(map, 'idle', function () {
            //alert(map.getBounds());
            var bb = map.getBounds();
            var ne = bb.getNorthEast(); // top-left
            var sw = bb.getSouthWest(); // bottom-right
            load_search_result(ne ,sw);
        });


    }

    function load_search_result(ne, sw){
        $('.leftPaneLoading').show();
        var loadUrl = '<?php echo site_url('show/map_search'); ?>';

        jQuery.post(
            loadUrl,
            {ne_lat: ne.lat(), ne_lng: ne.lng(), sw_lat: sw.lat(), sw_lng: sw.lng()},
            function (result) {
//                        alert(result);
                $('.leftPaneLoading').hide();
                result_js(result);
            },
            'json'
        );
    }


    function pointMap(map_data) {
            setAllMap(null);

        markers = [];
        var marker, i;

        var infoContentString = [];

        for (i = 0; i < map_data.estates.length; i++) {
            var infowindow = new google.maps.InfoWindow({
                content: "Hello World"
            });
            if (map_data.estates[i].estate_type == 'DBC_TYPE_COMSPACE') {
                var icon_path = iconBase + 'office.png';
            }
            else if (map_data.estates[i].estate_type == 'DBC_TYPE_HOUSE' || map_data.estates[i].estate_type == 'DBC_TYPE_VILLA') {
                var icon_path = iconBase + 'bighouse.png';
            }
            else if (map_data.estates[i].estate_type == 'DBC_TYPE_LAND') {
                var icon_path = iconBase + 'land.png';
            }
            else {
                var icon_path = iconBase + 'apartment.png';
            }


            marker = new google.maps.Marker({
                position: new google.maps.LatLng(map_data.estates[i].latitude, map_data.estates[i].longitude),
                map: map,
                title: map_data.estates[i].estate_title,
                icon: icon_path
            });

            infoContentString[i] = '<div class="thumbnail thumb-shadow map-thumbnail">' + '<div class="property-header">'
             + '<a href="' + map_data.estates[i].detail_link + '"></a>' + '<img class="property-header-image" src="' + map_data.estates[i].featured_image_url + '" alt="" style="width:100%">'
             + '<div class="property-thumb-meta">' + '<span class="property-price">' + map_data.estates[i].estate_price + '</span>' + '</div></div>'
             + '<div class="caption">' + '<h4>' + map_data.estates[i].estate_title + '</h4>' + '<p>' + map_data.estates[i].estate_short_address + '</p>' +
             '<div class="grid-list"><span class="rtl-right left">Type:</span><span class="rtl-left right">' + map_data.estates[i].estate_type_lang + '</span></div>' +
             '<div class="grid-list"><span class="rtl-right left">Area:</span><span class="rtl-left right">' + map_data.estates[i].area + '</span></div>' +
             '<div class="grid-list property-utilities"><div title="Bathrooms" class="bathrooms rtl-left"><div class="content">' + map_data.estates[i].bath + '</div></div><div title="Bedrooms" class="bedrooms  rtl-right"><div class="content">' +
             map_data.estates[i].bedroom + '</div></div><div class="clearfix"></div></div><div class="grid-divider"></div><p><a href="' + map_data.estates[i].detail_link + '" class="btn btn-primary  btn-labeled">' + 'Details<span class="btn-label btn-label-right"><i class="fa fa-arrow-right"></i></span></a></p>' + '<div class="clearfix"></div></div></div>';



            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
//                    infowindow.setContent(infoContentString[i]);
//                    infowindow.open(map, marker);
//                    map.setCenter(this.getPosition());
//                    map.setZoom(10);
                     var loadUrl = '<?php echo site_url('show/map_search_detail'); ?>/' + map_data.estates[i].estate_id;

                     jQuery.post(
                     loadUrl,
                     {},
                     function (result) {
                     $('#grid-content').html(result);
                     $('.right-pane').show("slide", { direction: "right" }, 1000);
                     }

                     );
                }
            })(marker, i));
            createMarkerButton(marker, map_data.estates[i]);
            markers.push(marker);

        }
        markerCluster = new MarkerClusterer(map, markers);



    }

    function createMarkerButton(marker, estate) {
        //Creates a sidebar button
        var ul = document.getElementById("marker_list");
        var li = document.createElement("li");
        li.className = "listing-single-item";

            if(estate.featured == '1'){
                is_featured = '<span class="premium-listing"></span>'
            }
            else{
                is_featured = '';
            }
        var photo_url = 'http://mw2.google.com/mw-panoramio/photos/medium/27932.jpg';
        var inner_html = is_featured + '<span class="property-name">' + estate.estate_title + '</span>' +
            '<div class="property-details clearFix">' + '<div class="property-img floatLeft">' +
            '<img width="40" height="40" src="'+ estate.featured_image_url + '">' + '</div>' + '<div class="property-info floatLeft">' +
            '<div class="property-cost">' + estate.estate_price + '</div>' + '<div class="property-location">' +
            estate.estate_short_address + '</div>' + '<div class="property-utility">' + '<span class="bedroom">'+estate.bedroom+'</span><span class="bathroom">'+ estate.bath+'</span><span class="area">' +
            estate.area + '</span></div></div></div>';

        li.innerHTML = inner_html;

//            $("#marker_list").append(inner_html);
        ul.appendChild(li);
        //Trigger a click event to marker when the button is clicked.
        google.maps.event.addDomListener(li, "click", function(){


            google.maps.event.trigger(marker, "click");
        });
    }

    function setAllMap(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }

        if (typeof markerCluster != "undefined") {
            console.log(markerCluster);
            markerCluster.clearMarkers();
        }
//        markerCluster = new MarkerClusterer(map, markers);
    }
    google.maps.event.addDomListener(window, 'load', initialize);



</script>
<div class="map-search">
    <div id="filter_options" class="top_Section">
        <div class="button_section">
            <div class="left_button">
                <ul id="buy_rent" class="buy_rent">
                    <li data-project="false" data-search-intent="sale" class="listing-search select"><a href="javascript:void(0);">Buy</a></li>
                    <li data-project="false" data-search-intent="rent" class="listing-search"><a href="javascript:void(0);">Rent</a></li>
                    <li data-project="true" data-search-intent="sale" class="project-search"><a href="javascript:void(0);">Projects</a></li>
                    <li class="project-search"><a href="javascript:void(0);">View on Map</a></li>
                </ul>
            </div>
            <div class="right_button"><a class="listview" onclick="_gaq.push(['_trackEvent', 'Map Search V3', 'Switch to List View']);" href="/apartments-for-sale">Switch to List View</a></div>
        </div>
        <div class="search_filter">

        </div>
    </div>
    <div class="map_section">

        <div class="left-pane">
            <div class="close_left_pane">
                <span class="close_left_pane_icon icon"></span>
            </div>

            <div class="">
                <div class="listing-no clearFix">
                    <div class="count-listing">
                        <span id="estate-count"></span> Results
                    </div>

                </div>

            </div>
            <div id="content-details" class="content-pane">
                <div class="left-pane-data">
                    <ul id="marker_list" class="listing-details">

                    </ul>
                </div>
            </div>
            <div class="leftPaneLoading" style="display: none;">
                <img alt="Loading..." src="<?php echo theme_url();?>/assets/images/loadmore.gif">
            </div>
        </div>
        <div id="main-map" class="main-map-holder"></div>

        <div class="right-pane" style="display: none">
            <a href="javascript:void(0);" class="close-icon"></a>
            <div id="grid-content">

            </div>

        </div>
    </div>
</div>





<script type="text/javascript">
    $( document ).ready(function() {

        $('.close-icon').click(function(){
            $('.right-pane').hide("slide", { direction: "right" }, 1000);
        });


        $('.close_left_pane').click(function(){
            var parent = $(this);
            var child = $('.close_left_pane > .icon');
            if(child.hasClass('close_left_pane_icon')){

                $(".left-pane").animate({left: "-270px"}, "slow", function () {
                    child.removeClass('close_left_pane_icon');
                    child.addClass('open_left_pane_icon')
                    parent.css('padding-left', '5px');
                });
            }
            else
            {
                $(".left-pane").animate({left: "2px"}, "slow", function () {
                    child.addClass('close_left_pane_icon')
                    child.removeClass('open_left_pane_icon');
                    parent.css('padding-left', '0px');
                });
            }

        });

        $(window).load(function(){
            $(".content-pane").mCustomScrollbar();
        });

    });
</script>