<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Memento admin Controller
 *
 * This class handles user account related functionality
 *
 * @package		Show
 * @subpackage	Show
 * @author		dbcinfotech
 * @link		http://dbcinfotech.net
 */
require_once 'show_core.php';
class Show extends Show_core {

	public function __construct()
	{
		parent::__construct();
	}

	public function import_bulk_location()
	{
		die;
		ini_set('max_execution_time', 7200);
		$this->load->model('admin/realestate_model');

		$json_url  = theme_url().'/assets/js/sample_estates.json';
		$str = file_get_contents($json_url);
		$map_data = json_decode($str, true);

		foreach($map_data['estates'] as $estate)
		{
			$test_latitude = $estate['latitude'];
			$test_longitude = $estate['longitude'];

			$details_url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=".$test_latitude.",".$test_longitude."&sensor=false";

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $details_url);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$response = json_decode(curl_exec($ch), true);

			curl_close($ch);

			// If Status Code is ZERO_RESULTS, OVER_QUERY_LIMIT, REQUEST_DENIED or INVALID_REQUEST

			if ($response['status'] != 'OK') {

				echo '<pre>' . print_r('Geocoding not working', true) . '</pre>';

			}
			else{

				$addresses = $response['results'][0]['address_components'];
				$addresses = array_reverse($addresses);

				echo '<pre>' . print_r($addresses, true) . '</pre>';


				foreach($addresses as $address){

					if($address['types'][0] == 'country'){

						$country_id = $this->realestate_model->get_location_id_by_name($address['long_name'],'country',0);
					}
					elseif($address['types'][0] == 'administrative_area_level_1'){
						$state_id = $this->realestate_model->get_location_id_by_name($address['long_name'],'state',$country_id);
					}
					elseif($address['types'][0] == 'administrative_area_level_2'){
						$city_id = $this->realestate_model->get_location_id_by_name($address['long_name'],'city',$state_id);
					}
					elseif($address['types'][0] == 'route'){
						$estate_address = $address['long_name'];
					}
					elseif($address['types'][0] == 'postal_code'){
						$postal_code = $address['long_name'];
					}
				}
				$data = array();
				$data['unique_id']		= uniqid();
				$data['type'] 			= 'DBC_TYPE_HOUSE';
				$data['purpose'] 		= 'DBC_PURPOSE_SALE';
				$data['total_price'] 	= 5000;
				$data['price_per_unit'] = 500;
				$data['home_size'] 		= 5000;
				$data['home_size_unit'] = 'sqft';
				$data['bedroom'] 		= 5;
				$data['bath'] 			= 3;
				$data['year_built'] 	= 1989;
				$data['estate_condition'] 		= 'DBC_CONDITION_NEW';
				$data['address'] 		= $estate_address;
				$data['country'] 		= $country_id;
				$data['state'] 			= $state_id;
				$data['city'] 			= $city_id;
				$data['zip_code'] 		= $postal_code;
				$data['latitude'] 		= $test_latitude;
				$data['longitude'] 		= $test_longitude;
				$data['facilities'] 	= '["1","2","3","6","7","9","10","13"]';
				$data['featured_img'] 	= '3589652595_115056110c.jpg';

				$this->load->helper('date');
				$time = time();
				$data['create_time'] 	= $time;
				$data['created_by']	= 1;
				$data['status']	= 1;
				$id = $this->realestate_model->insert_estate($data);

				$data = array();
				$data['post_id'] 	= $id;
				$data['key']		= 'title';
				$data['status']	= 1;
				$data['value'] = '{"en":"Sweet Home","es":"Sweet Home","ru":"Sweet Home","ar":"Sweet Home","tr":"Sweet Home","nl":"Sweet Home","fr":"Sweet Home","pt":"Sweet Home"}';
				$this->realestate_model->insert_estate_meta($data);

				$data = array();
				$data['post_id'] 	= $id;
				$data['key']		= 'description';
				$data['status']	= 1;
				$data['value'] = '{"en":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","es":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","ru":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","ar":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","tr":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","nl":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","fr":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing \npurus vehicula.<br>","pt":"Aliquam vel egestas turpis. Proin sollicitudin imperdiet nisi ac \nrutrum. Sed imperdiet libero malesuada erat cursus eu pulvinar tellus \nrhoncus. Ut eget tellus neque, faucibus ornare odio. Fusce sagittis \nhendrerit mi a sollicitudin.\nLorem ipsum dolor sit amet, consectetur adipiscing elit. \nEtiam ullamcorper libero sed ante auctor vel gravida nunc placerat. \nSuspendisse molestie posuere sem, in viverra dolor venenatis sit amet. \nAliquam gravida nibh quis justo pulvinar luctus. Phasellus a malesuada \nmassa. Mauris elementum tempus nisi, vitae ullamcorper sem ultricies \nvitae. Nullam consectetur lacinia nisi, quis laoreet magna pulvinar in. \nClass aptent taciti sociosqu ad litora torquent per conubia nostra, per \ninceptos himenaeos. In hac habitasse platea dictumst. Cum sociis natoque\n penatibus et magnis dis parturient montes, nascetur ridiculus mus.Morbi\n eu sapien ac diam facilisis vehicula nec sit amet odio. Vivamus quis \ndui ac nulla molestie blandit eu in nunc. In justo erat, lacinia in \nvulputate non, tristique eu mi. Aliquam tristique dapibus tempor. \nVivamus malesuada tempor urna, in convallis massa lacinia sed. Phasellus\n gravida auctor vestibulum. Suspendisse potenti. In tincidunt felis \nbibendum nunc tempus sagittis. Praesent elit dolor, ultricies interdum \nporta sit amet, iaculis in neque. Nullam urna ante, tempus vel iaculis \nnec, rutrum sit amet nulla. Morbi vestibulum ante in turpis ultricies in\n tincidunt sapien iaculis. Aenean feugiat rhoncus arcu, at luctus libero\n blandit tempus. Vivamus rutrum tellus quis leo placerat eu adipiscing purus vehicula."}';
				$this->realestate_model->insert_estate_meta($data);



			}
		}
		die;


	}
}
/* End of file install.php */
/* Location: ./application/modules/show/controllers/show.php */